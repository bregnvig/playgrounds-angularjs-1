angular.module('playgrounds', ['ngRoute', 'ngResource', 'leaflet-directive'])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'map.html',
        controller: 'mapsCtrl'
      })
      .when('/playground/:id', {
        templateUrl: 'map.html',
        controller: 'mapsCtrl'
      })
      .when('/details/:id', {
        templateUrl: 'details.html',
        controller: 'ratingCtrl'
      })
      .otherwise( {
        redirectTo: '/'
      } );
  })
  .run(function($rootScope, $window) {
    $window.navigator.geolocation.getCurrentPosition(function(position) {
      console.log('Got current position', position.coords);
      $rootScope.$apply(function() {
        $rootScope.$broadcast('position-determined', position.coords);
      });
    }, function() {
      console.log('Unable to get current position');
    });
  })
  .controller('playgroundsCtrl', function($scope, $location, playgroundService) {
    function setActive(selectedPlaygroundId) {
      if (!selectedPlaygroundId) return;
      playgroundService.playgrounds.forEach(function(playground) {
        playground.selected = playground.id === selectedPlaygroundId;
      });
    }
    $scope.$on('position-determined', function () {
      $scope.playgrounds = playgroundService.playgrounds;
      setActive($location.url().slice($location.url().lastIndexOf('/')+1));
    });

    $scope.changePlayground = function(id) {
      $location.path('/playground/'+id);
      setActive(id);
    };
  }).
  controller('mapsCtrl', function($scope, $routeParams, playgroundService, Playground) {

    $scope.playground = playgroundService.find($routeParams.id);
    $scope.playgrounds = playgroundService.playgrounds;
    $scope.$watch('playgrounds', function(value) {
      if ($routeParams.id && (!$scope.playground || $scope.playground.id !== $routeParams.id)) {
        showPlayground();
      }
    }, true);
    $scope.$on('position-determined', function (event, coordinates) {
      $scope.markers.meMarker = {
        lat: coordinates.latitude,
        lng: coordinates.longitude,
        message: 'Her er jeg',
        draggable: false
      };
    });
    $scope.markers = { };
    $scope.playgroundCenter = {
      lat: 56.360029,
      lng: 10.746635,
      zoom: 8
    };
    function showPlayground() {
      $scope.playground = playgroundService.find($routeParams.id);
      if ($scope.playground) {
        $scope.rating = Playground.get({id: $routeParams.id});
        $scope.playgroundCenter = {
          lat: $scope.playground.position.latitude,
          lng: $scope.playground.position.longitude,
          zoom: 17
        };
        $scope.markers.playgroundMarker = {
          lat: $scope.playground.position.latitude,
          lng: $scope.playground.position.longitude
        };
      }
    }
    showPlayground();
  })
  .controller('ratingCtrl', function($scope, $route, $routeParams, playgroundService, Playground, Rating) {
    $scope.playgrounds = playgroundService.playgrounds;
    $scope.$watch('playgrounds', function() {
      if ($routeParams.id) {
        showPlayground();
      }
    }, true);
    $scope.model = {
      rating: 0
    };

    $scope.ratings = Rating.query({'id': $routeParams.id});

    $scope.createRating = function() {
      var rating = new Rating(
        {
          'comment': $scope.model.comment,
          'rating': $scope.model.rating
        });
      rating.$save({id:$routeParams.id},
        function() {
        $scope.model = {
          'rating': 0
        };
        $scope.ratings = Rating.query({'id': $routeParams.id});
        $scope.rating = Playground.get({'id': $scope.playground.id});
      });
    };

    function showPlayground() {
      $scope.playground = playgroundService.find($routeParams.id);
      console.log('Playground', $scope.playground);
      if ($scope.playground) {
        $scope.rating = Playground.get({'id': $scope.playground.id});
        $scope.rating.$promise.then(null, function(response) {
          if (response.status === 404) {
            $scope.rating.identifier = $scope.playground.id;
            $scope.rating.lng = $scope.playground.position.longitude;
            $scope.rating.lat = $scope.playground.position.latitude;
            $scope.rating.$save();
          }
        });
      }
    }
  });
