angular.module('playgrounds')
  .filter('defaultDescription', function() {

    return function(value) {
      return value || 'Ingen beskrivelse';
    };
  }).
  filter('distance', function($rootScope) {
    var currentPosition;
    $rootScope.$on('position-determined', function(event, coordinates) {
      currentPosition = {
        'latitude': coordinates.latitude,
        'longitude': coordinates.longitude
      };
    });
    return function(position) {
      if (position.latitude && position.longitude && currentPosition) {
        return geolib.getDistance(currentPosition, position);
      }
      return undefined;
    };
  }).
  filter('humanizeDistance', function() {
    return function(distance) {
      if (distance < 750) {
        return distance + ' m';
      } else if(distance < 2000) {
        return 'Et stykke vej';
      } else if (angular.isNumber(distance)) {
        return 'Ikke til fods!';
      } else {
        return 'Ukendt';
      }
    };
  });
