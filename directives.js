angular.module('playgrounds')
  .directive('playgroundInfoBox', function() {


    return {
      restrict: 'E',
      templateUrl: 'info-box.html',
      scope: {
        playground: '=',
        rating: '='
      },
      controller: function($scope, $location) {
        $scope.showRating = function(playground) {
          $location.path('/details/'+playground.id);
        };
      }
    };
  })
  .directive('myRating', function() {
    return {
      restrict: 'E',
      templateUrl: 'my-rating.html',
      scope: {
        rating: '=',
        readOnly: '='
      },
      link: function($scope, $element) {
        $scope.$watch('rating', function () {
          for(var i = 1; i <= 5; i++) {
            var star = $element.find('#rating'+i);
            star.removeClass('fa-star fa-star-o fa-star-half-o');
            if ($scope.rating - i >= 0) {
              star.addClass('fa-star');
            } else if ($scope.rating - i >= -0.5) {
              star.addClass('fa-star-half-o');
            } else {
              star.addClass('fa-star-o');
            }
          }
        });
      },
      controller: function($scope) {
        $scope.change = function(value) {
          if (!$scope.readOnly) {
            $scope.rating = value;
          }
        };
      }
    };
  })
  .directive('rating', function() {
    return {
      restrict: 'E',
      templateUrl: 'rating.html',
      scope: {
        message: '=',
        rating: '='
      }
    };
  });
