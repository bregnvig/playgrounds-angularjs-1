angular.module('playgrounds')
  .factory('playgroundService', function($http, $rootScope) {

    var playgrounds = [];
    var lookup = {};
    var currentPosition;

    $http({
      url: 'http://data.kk.dk/dataset/legepladser/resource/79d60521-5748-4287-a875-6d0e23fac31e/proxy',
      method: 'GET',
      isArray: true,
      transformResponse: function(data, headersGetter, status) {
        if (status === 200) {
          var opendata = angular.fromJson(data);
          return opendata.features.map(function(openPlayground) {
            return {
              id: openPlayground.id,
              navn: openPlayground.properties.navn,
              adresseBeskrivelse: openPlayground.properties.adressebeskrivelse,
              beskrivelse: openPlayground.properties.beskrivelse,
              position: {
                'latitude': openPlayground.geometry.coordinates[1],
                'longitude': openPlayground.geometry.coordinates[0]
              }
            };
          });
        } else {
          console.log('Unable to fetch opendata', status);
        }
      }
    })
    .success(function(data) {
      angular.copy(data, playgrounds);
      data.forEach(function(playground) {
        lookup[playground.id] = playground;
      });
      if (currentPosition) {
        playgrounds.sort(sortByDistance);
      }
    });

    function sortByDistance(a, b) {
      return geolib.getDistance(currentPosition, a.position) - geolib.getDistance(currentPosition, b.position);
    }

    $rootScope.$on('position-determined', function (event, coordinates) {
      currentPosition = {
        'latitude': coordinates.latitude,
        'longitude': coordinates.longitude
      };
      playgrounds.sort(sortByDistance);
    });

    return {
      playgrounds: playgrounds,
      find: function(id) {
        return lookup[id];
      }
    };
  })
  .factory('Playground', function($resource) {
      //return $resource('http://localhost:8080/location/:id');
      return $resource('https://ratr-2015.appspot.com/location/:id');
  })
  .factory('Rating', function($resource) {
      //return $resource('http://localhost:8080/location/:id/rating');
      return $resource('https://ratr-2015.appspot.com/location/:id/rating');
  });
